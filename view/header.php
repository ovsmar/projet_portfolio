<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Mon portfoilo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mon Portfolio">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Simonian Ovsep">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- <link rel="preconnect" href="https://fonts.googleapis.com"> -->
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet/less" type="text/css" href="../css/styles.less" />

</head>
<!-- Header -->

<header>

    <div class="container">
        <div class="intro-text">

            <div class="intro-heading">Ovsep Simonian</div>

            <i id="arrow" class='fas fa-angle-down arrow' style='font-size:36px; color:white'></i>
        </div>
    </div>
</header>
<!--End Header-->

<body id="page-top" class="index">