<?php
include 'secured.php';
include '../model/data.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php include ("headerAdmin.php") ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/add.css">

    <title>Formulaire</title>
</head>

<body>


    <h1>Ajouter projet</h1>
    <form enctype="multipart/form-data" action="../controller/insert.php" method="post">
        <div class="card">
            <h1> <u>Card</u> </h1>

            <div class="bord2">
                <label for="card_image"> Image: </label>
                <input type="file" id="card_image" name="card_image" accept="image/png, image/jpeg">
            </div>

            <div class="bord2">
                <label for="card_title"> Title: </label>
                <input type="text" name="card_title">
            </div>

            <div class="bord2">
                <label for="card_text"> Text: </label>
                <input type="text" name="card_text">
            </div>


            <h1> <u>Modal</u> </h1>

            <div class="bord2">
                <label for="modal-title"> Modal Title: </label>
                <input type="text" name="modal_title">
            </div>

            <div class="bord2">
                <label for="img_modal"> Modal image: </label>
                <input type="file" name="img_modal">
            </div>

            <div class="bord2">
                <label for="modal_titre"> Modal Title: </label>
                <input type="text" name="modal_titre">
            </div>

            <div class="bord2">
                <label for="modal_text"> Modal text: </label>
                <input type="text" name="modal_text">
            </div>

            <div class="bord2">
                <label for="modal_date"> Date: </label>
                <input type="date" name="modal_date">
            </div>

            <H1>COMPÉTENCS </H1>
            <div class="compGlobal">
                <?php
        
            $list_comp = getOnlyComp();
            foreach ($list_comp as $comp) {?>
                <div class="comp">
                    <input type="checkbox" name="<?php echo $comp['nom'] ?>">
                    <label for="checkbox"><?php echo $comp['nom'] ?></label>

                </div>
                <?php } ?>
            </div>

            <H1>Technos</H1>
            <div class="techno">
                <?php
    
        $list_techno = getOnlyTechno();
        
        foreach ($list_techno as $techno) {?>
                <div class="bord">
                    <div class="technos">
                        <input type="checkbox" name="<?php echo $techno['nom'] ?>">
                        <label class="fontTechno" for="checkbox"><?php echo $techno['nom'] ?></label>
                    </div>

                    <div class="delt">
                        <form class="test" action="../controller/deleteTechno.php" method="post">

                            <button type="submit" name="supp" value="<?php echo $techno['id']?>"><i
                                    class="fas fa-trash"></i></button>
                        </form>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="bord2">

                <form class="ajouterTechno" method="post" action="../controller/addtechno.php">

                    <label for="ajtech"><u>Ajouter une Technologie</u></label>
                    <input type="text" name="ajtech" id="ajtech">

                    <input type="submit" value="Ajouter">

                </form>
            </div>

            <div class="bord2">
                <label for="lien_git"> <u>Lien git:</u></label>
                <input type="text" name="lien_git">
            </div>

            <div class="bord2">
                <label for="lien"> <u>Lien site:</u></label>
                <input type="text" name="lien">
            </div>

            <input type="submit" value="Ajouter">


            <button class="btn"><a href="supprimer.php">Supprimer/Modifier Projet</a></button>
        </div>

    </form>




    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
</body>

</html>