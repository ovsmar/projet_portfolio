<?php
include '../model/data.php';
include 'secured.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php include ("headerAdmin.php") ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="../css/update.css">
    <title>Document</title>
</head>

<body>

</body>

</html>
<?php
    $id = $_POST['modif'];
    if(isset($_POST['modif'])){
        $functionlist_pro = getProjectId($id);
        $functionlist_comp = getOnlyComp($id);
        $functionlist_compID =getAllComp($id);
        
    }
    
    foreach($functionlist_pro as $f){
?>
<h1>Modifier </h1>
<form enctype="multipart/form-data" action="../controller/update.php" method="post">
    <div class="card">
        <h1> <u>Card</u> </h1>

        <div class="bord2">

            <img src="img/<?php echo $f['card_image']?>">
            <label for="card_image"> card_image: </label>
            <input name="card_image" id="card_image" type="file" value="<?php echo $f['card_image']?>">

        </div>

        <div class="bord2">
            <label for="card_title"> Title: </label>
            <input type="text" name="card_title" value="<?php echo $f ['card_title']?>">
        </div>

        <div class="bord2">
            <label for="card_text"> Text: </label>
            <input type="text" name="card_text" value="<?php echo $f ['card_text']?>">
        </div>


        <h1> <u>Modal</u> </h1>

        <div class="bord2">
            <label for="modal-title"> Modal Title: </label>
            <input type="text" name="modal_title" value="<?php echo $f ['modal_title']?>">
        </div>

        <div class="bord2">

            <img src="img/<?php echo $f['img_modal']?>">
            <label for="img_modal"> img_modal: </label>
            <input name="img_modal" id="img_modal" type="file" value="<?php echo $f['img_modal']?>">
        </div>

        <div class="bord2">
            <label for="modal_titre"> Modal Title: </label>
            <input type="text" name="modal_titre" value="<?php echo $f ['modal_titre']?>">
        </div>

        <div class="bord2">
            <label for="modal_text"> Modal text: </label>
            <input type="text" name="modal_text" value="<?php echo $f ['modal_text']?>">
        </div>

        <div class="bord2">
            <label for="modal_date"> Date: </label>
            <input type="date" name="modal_date" value="<?php echo $f ['modal_date']?>">
        </div>




        <div class="bord2">
            <label for="lien_git"> <u>Lien git:</u></label>
            <input type="text" name="lien_git" value="<?php echo $f ['lien']?>">
        </div>

        <div class="bord2">
            <label for="lien"> <u>Lien site:</u></label>
            <input type="text" name="lien" value="<?php echo $f ['lien']?>">
        </div>


        <button class="btn btn-outline-primary btn-sm mt-2" type="submit" name="id"
            value="<?php echo $f['id'];?>">Modifier </button>
</form>
<?php } ?>