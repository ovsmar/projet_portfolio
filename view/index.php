<?php
    session_start();
    $_SESSION = array();
    session_destroy();
?>


<?php include ("header.php") ?>



<a id="myBtn" class="fixed-button wobble" href="#openModal">
    <p class="modalaboutme">?</p>
</a>

<div id="openModal" class="modalbg">
    <div class="dialog">
        <a href="#close" title="Close" class="close">X</a>
        <h2>Ovsep Simonian</h2>
        <p>Apprenant Développeur Web!</p>
        <p>Je m'appelle Ovsep Simonian et j'ai 21 ans.
            Actuellement, en formation de développeur web et web mobile
            chez <a href="https://simplon.co/"><u>Simplon</u></a></p>
        <p class="fineprint"><a href="#section3"><u>CONTACT ME</u></a></p>
        <p class="fineprint">:)</p>
    </div>
</div>



<div class="preloader">
    <div class="loading">
        <img src="../img/giphy.gif" width="500">
        <!-- <img src="https://media.giphy.com/media/l3q2XyZgnd2qzwx6o/giphy.gif" width="500"> -->
        <!-- <img src="https://media.giphy.com/media/5eLDrEaRGHegx2FeF2/giphy.gif" width="500"> -->
    </div>
</div>


<!-- Navigation 
-->
<nav class="navbar navbar-default navbar-fixed-top navbar-shrink">
    <div class="container">
        <!--Бренд и переключатель сгруппированы для лучшего отображения на мобильных устройствах -->

        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">Home</a>
        </div>

        <!--Собирайте навигационные ссылки, формы и другой контент для переключения -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden active">
                    <a href="#page-top"></a>
                </li>
                <li class="">
                    <a class="page-scroll" href="#section1">Portfolio</a>
                </li>
                <li class="">
                    <a class="page-scroll" href="#section2"> Mes Compétence</a>
                </li>
                <li class="">
                    <a class="page-scroll" href="#section3">Contact</a>
                </li>
                <li class="">
                    <a class="page-scroll" target="_blank" href="../img/cv.pdf">Cv</a>
                </li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Section1 -->

<div id="section1" class="container-fluid">
    <div class="main">
        <h1>COMPÉTENCS<span> & </span>PORTFOILO
            <hr />
        </h1>


        <!-- Radio -->
        <!--часть кликабельной части -->

        <div class="warpper">
            <input class="radio" id="one" name="group" type="radio">
            <input class="radio" id="two" name="group" type="radio">
            <input class="radio" id="three" name="group" type="radio">
            <input class="radio" id="four" name="group" type="radio">
            <input class="radio" id="five" name="group" type="radio">
            <input class="radio" id="six" name="group" type="radio">
            <input class="radio" id="seven" name="group" type="radio">
            <input class="radio" id="eight" name="group" type="radio">

            <!-- Tabs -->
            <!--часть кликабельной части -->

            <div class="tabs">
                <label class="tab" id="one-tab" for="one">C1</label>
                <label class="tab" id="two-tab" for="two">C2</label>
                <label class="tab" id="three-tab" for="three">C3</label>
                <label class="tab" id="four-tab" for="four">C4</label>
                <label class="tab" id="five-tab" for="five">C5</label>
                <label class="tab" id="six-tab" for="six">C6</label>
                <label class="tab" id="seven-tab" for="seven">C7</label>
                <label class="tab" id="eight-tab" for="eight">C8</label>
            </div>
            <!-- End tabs -->


            <!-- Panels -->
            <!--часть панели с текстом -->

            <div class="panels">
                <div class="panel" id="one-panel">
                    <div class="panel-title">Compétence C1</div>
                    <p>Maquetter une application</p>
                </div>

                <div class="panel" id="two-panel">
                    <div class="panel-title">Compétence​ C2</div>
                    <p> Réaliser une interface utilisateur web statique et adaptable!</p>
                </div>

                <div class="panel" id="three-panel">
                    <div class="panel-title">Compétence​ C3</div>
                    <p>Développer une interface utilisateur web dynamique</p>
                </div>

                <div class="panel" id="four-panel">
                    <div class="panel-title">Compétence​ C4</div>
                    <p>Réaliser une interface utilisateur avec
                        une solution de gestion de contenu ou e-commerce</p>
                </div>

                <div class="panel" id="five-panel">
                    <div class="panel-title">Compétence C5</div>
                    <p>Créer une base de données</p>
                </div>

                <div class="panel" id="six-panel">
                    <div class="panel-title">Compétence​ C6</div>
                    <p>Développer les composants d’accès aux données</p>
                </div>

                <div class="panel" id="seven-panel">
                    <div class="panel-title">Compétence C7</div>
                    <p>Développer la partie back-end d’une
                        application web ou web mobile</p>
                </div>

                <div class="panel" id="eight-panel">
                    <div class="panel-title">Compétence C8</div>
                    <p>Elaborer et mettre en œuvre des composants
                        dans une application de gestion de contenu ou e-commerce</p>
                </div>

            </div>
        </div>
        <!--  End panels -->

        <!-- Card 1-->
        <!--карта под номером 1 -->
    </div>
</div>


<?php 
      include '../model/data.php';
      $list_pro = getAllPro();
      
      ?>

<div id="sectioncard" class="container-fluid">
    <div class="main row ">
        <?php 
      foreach ($list_pro as $f) {
        ?>
        <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="cards">
                <div class="cards_item">
                    <div class="card">
                        <div class="card_image">
                            <div class="card_image img-fluid"><img src="../img/<?php echo $f["card_image"]?>"
                                    alt="deadcowdiner" class="card_image"></div>
                        </div>
                        <div class="card_content">
                            <h2 class="card_title"><?php echo $f["card_title"]?></h2>
                            <p class="card_text"><?php echo $f["card_text"]?></p>

                            <!-- Modal-->
                            <!-- Модальное окно для первой карты -->

                            <button href="#myModal<?= $f["id"]?>" class="btn card_btn" data-backdrop="false"
                                data-toggle="modal">Read more</button>
                            <div id="myModal<?= $f["id"]?>" class="modal fade ">
                                <div class="modal-dialog classA">
                                    <div class="modal-content">
                                        <div class="modal-header">

                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"><?= $f["modal_title"]?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="img_modal"> <img id="mobileImg"
                                                    src="../img/<?php echo $f["img_modal"]?>" alt="deadcowdiner"
                                                    class="centrer"></div>
                                            <h1 class="modal_titre"><?php echo $f["modal_titre"]?></h1>
                                            <h4 class="modal_text "><?php echo $f["modal_text"]?></h4>
                                            <p class="modal_date"><u>Date:</u><?php echo $f["modal_date"]?></p>


                                            <p class="modal_comp"><u>Compétences</u>
                                                <?php $list_comp = getAllComp($f["id"]);
                                    foreach($list_comp as $truc) {
                                      echo $truc["nom"].", ";
                                    }?>
                                            </p>

                                            <p class="modal_technos"><u>Technos:</u>
                                                <?php $list_techno = getAllTechno($f["id"]);
                                    foreach($list_techno as $truc2) {
                                      echo $truc2["nom"].", "; 
                                    }?>
                                            </p>
                                            <a class="lien_git" href="<?php echo $f["lien_git"]?>"><i
                                                    class="fab fa-gitlab"><u>Git</u></i></a><br>
                                            <a class="lien" href="<?php echo $f["lien"]?>"><i
                                                    class="fas fa-link"><u>Lien</u></i></a>
                                        </div>

                                        <div class="modal-footer">

                                            <button type="button" class="btn card_btn"
                                                data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <?php } ?>
    </div>
</div>
<!-- End Card 1-->
<!-- Close Section1 -->

<!-- Section2 -->

<div id="section2" class="container-fluid">
    <div class="sec2h1">

    </div>
    <!--start my skils-->
    <div class="my-skils">
        <div class="containerskils">
            <h1 class="sec2h1color">Mes Compétence</h1>
            <div class="skils">

            </div>

            <div class="skills-diagram">
                <div class="item skill-1 ">Git</div>

                <div class="item skill-2">css</div>

                <div class="item skill-3">BOOTSTRAP</div>


                <div class="item skill-4"><span style="font-size:.8em">HTML5</span></div>

                <div class="item skill-5">Symfony</div>


                <div class="item skill-6">Python</div>


                <!-- <br> -->
                <div class="item skill-7">sql</div>

                <div class="item skill-8">php</div>

                <div class="item skill-9">Javascript</div>
                <div class="item skill-10"><span style="font-size:.7em">Wordpress</span></div>
                <div class="item skill-10"><span style="font-size:.7em">GIT</span></div>
                <br>
            </div>
        </div>

    </div>
</div>

<!--end my skils-->
<!-- Close Section2 -->

<!-- Section3 -->

<div id="section3" class="container-fluidd">
    <h1>CONTACT ME</h1>
    <!-- Contact forme -->
    <form action="../controller/contact.php" method="post" onsubmit="return checkForm(this);">
        <cite>* Champs obligatoires</cite>
        <input name="nom" type="text" class="feedback-input" placeholder="Name" maxlength="25" autocomplete="off"
            required />
        <input name="email" type="email" class="feedback-input" placeholder="Email"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autocomplete="off" required />
        <textarea name="comment" class="feedback-input" placeholder="Comment" maxlength="255" autocomplete="off"
            required></textarea>



        <div class="elem-group">
            <div class="rowCap">
                <label for="captcha">Please Enter the Captcha Text</label>
                <img src="../controller/captcha.php" alt="CAPTCHA" class="captcha-image"><i
                    class="fas fa-redo refresh-captcha"></i>
            </div>
            <br>
            <div class="CapValide">
                <p><input class="captchaInput" id="captcha_code_input" type="text" name="captcha" size="6" maxlength="5"
                        onkeyup="this.value = this.value.replace(/[^\d]+/g, '');"> </p>
            </div>
            <div id="msg">
                <?php 
            if(isset($_GET['msg'])){
                echo"(".$_GET['msg'].")";
                // echo"<script>alert(".$_GET['msg'].")</scrip>";
            }?>
            </div>
        </div>

        <input type="submit" value="SUBMIT" />




    </form>


</div>
<!-- Close Section3" -->

<?php include ('footer.php') ?>

</body>

</html>