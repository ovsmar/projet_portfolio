<!DOCTYPE html>
<html lang="en">
<?php include ("headerAdmin.php") ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/inbox.css">
    <title>Document</title>
</head>

<body>



    <?php 
      include 'secured.php';
      include '../model/data.php';
      $list_mail = getAllMail();
      
      ?>


    <table class="GeneratedTable">
        <thead>
            <tr>
                <th>ID</th>
                <th>nom</th>
                <th>email</th>
                <th>comment</th>
                <th>Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php 
      foreach ($list_mail as $f) {
        ?>
            <tr>
                <td><?php echo $f["id"]?></td>
                <td><?php echo $f["nom"]?></td>
                <td><a href="mailto: abc@example.com"><?php echo $f["email"]?></a></td>
                <td><?php echo $f["comment"]?></td>
                <td>
                    <div class="delt">
                        <form class="test" action="../controller/deleteMail.php" method="post">

                            <button type="submit" name="supp" value="<?php echo $f['id']?>"><i
                                    class="fas fa-trash"></i></button>
                        </form>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
</body>

</html>