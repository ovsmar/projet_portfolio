<?php
include 'secured.php';

?>

<!DOCTYPE html>
<html lang="en">
<?php include ("headerAdmin.php") ?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link type="text/css" rel="stylesheet" href="../css/supprimer.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <h1>LISTE DE PROJETS:</h1>
    <div class="container">
        <?php
    include '../model/data.php';

    $list_pro = getAllPro();
    ?>
        <?php foreach ($list_pro as $f) {?>
        <div class="bord">



            <div class="projet">

                <p><?php echo $f["card_title"]; ?> / <?php echo $f["id"]; ?></p>
            </div>
            <div class="delt">
                <a href="../controller/supprequete.php? id=<?php echo $f["id"]; ?>" name="supp"><i
                        class="fas fa-trash"></i></a>

                <form action="formUpdate.php" method="post">
                    <button type="submit" name="modif" value="<?php echo $f['id']?>">Modifier</button>
                </form>


            </div>
        </div>

        <?php } ?>
    </div>



</body>
<script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>

</html>