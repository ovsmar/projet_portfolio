<!-- Footer -->

<footer>
    <div class="footer-links">
        <ul class="footer-links-social">
            <li class="footer-social-icon"><a href="https://codepen.io/ovsmar" aria-label="codepen"><i
                        class="fab fa-codepen"></i></a></li>
            <li class="footer-social-icon"><a href="https://gitlab.com/ovsmar" aria-label="gitlab"><i
                        class="fab fa-gitlab"></i></a></li>
            <li class="footer-social-icon"><a href="https://fr.linkedin.com/in/ovsep-simonian-43082b216"
                    aria-label="linkedin"><i class="fab fa-linkedin"></i></a></li>
        </ul>
        <ul class="footer-links-menu">
            <li><a href="#page-top">HOME</a></li>
            <li><a href="#section1">PORTFOILO</a></li>
            <li><a href="#section2">MES COMPÉTENCE</a></li>
            <li><a href="#section3">CONTACT</a></li>
            <li><a target="_blank" href="../img/cv.pdf">CV</a></li>
        </ul>


        <div class="arrowpagedown">
            <i id="arrow2" class='fas fa-angle-up arrow' style='font-size:36px; color:white'></i>
        </div>


    </div>
    <div class=" footer-legal">
        <p class="footer-made"><i class="fas fa-code"></i> <span> &copy; <u> 2022</u> Simonian Ovsep, Made with
                <span class="footer-heart"><i class="fas fa-heart"></i></span> in Narbonne. </span></p>

    </div>
</footer>

<!-- Script-->
<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/less@4"></script>
<script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
<script src="../js/index.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
    integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous">
</script>
<!--  Close script-->