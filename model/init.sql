DROP DATABASE IF EXISTS projet;
CREATE DATABASE projet;
USE projet;

CREATE TABLE cart (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    card_image VARCHAR(255),
    card_title VARCHAR(25),
    card_text VARCHAR(255),
    modal_title VARCHAR(25),
    img_modal  VARCHAR(255),
    modal_titre VARCHAR(25),
    modal_text  VARCHAR(255),
    modal_date VARCHAR(25),
    lien_git VARCHAR(255)
);

CREATE TABLE comp(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255)
);



CREATE TABLE techno(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255)
);

CREATE TABLE projetcomp(
    ID_projet INT,
    ID_comp INT
);

CREATE TABLE projettechno (
    ID_projet INT,
    ID_techno INT
);



GRANT ALL PRIVILEGES ON cart.* TO 'ovsep'@'localhost';