<?php
include 'debug.php';
$site_domain = "/index.php";
$host = 'localhost';
$db   = 'projet';
$user = 'ovsep';
$pass = 'password';

try{
$pdo = new PDO('mysql:host=localhost;dbname=projet', 'ovsep', 'password');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
catch(PDOException $e){
    echo $e->getMessage()
;}


function InsertProjet($card_image, $card_title,$card_text,$modal_title,$img_modal,$modal_titre,$modal_text,$modal_date,$lien_git,$comps,$technos,$lien, $ID_techno ,$ID_comp){
    global $pdo;
    try{
    $req = $pdo->prepare("INSERT INTO cart (card_image, card_title,card_text,modal_title,img_modal,modal_titre,modal_text,modal_date,lien_git,lien) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
    $req->execute([$card_image, $card_title,$card_text,$modal_title,$img_modal,$modal_titre,$modal_text,$modal_date,$lien_git,$lien]);
    $idProj = $pdo->lastInsertId();
    $req = $pdo->prepare("INSERT INTO projettechno values (?,?);");
    foreach ($ID_techno as $tech){
        $req->execute([$idProj, $tech]);
    }

    $req2 = $pdo->prepare("INSERT INTO projetcomp values (?,?);");
    foreach ($ID_comp as $comp){
        $req2->execute([$idProj, $comp]);
    }
}catch(Exception $e){
    // en cas d'erreur :
     echo " Erreur ! ".$e->getMessage();
     echo " Les datas : " ;
     print_r([$card_image, $card_title,$card_text,$modal_title,$img_modal,$modal_titre,$modal_text,$modal_date,$lien_git,$lien]);
    }

    
    $ID_projet= $pdo->lastInsertId();


foreach ($comps as $ID_comp ){
    if ($ID_comp ){
        $req = $pdo->prepare("INSERT INTO projetcomp (ID_projet, ID_comp) VALUES (?,?);");
        $req->execute([$ID_projet, $ID_comp]);
    }
}

foreach ($technos as $ID_techno){
    if($ID_techno){
        $req = $pdo->prepare("INSERT INTO projettechno (ID_projet, ID_techno) VALUES (?,?);");
        $req->execute([$ID_projet, $ID_techno]);
    }
}

};
function getAllPro(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM cart");
    return $req->fetchAll();
};

function addTechno($ajtech){
    global $pdo;
    $req = $pdo->prepare("INSERT into techno (nom) values (?);");
    $req->execute([$ajtech]);
};


function getAllComp($id){
    global $pdo;
    $req = $pdo->prepare('SELECT comp.nom FROM comp, projetcomp WHERE projetcomp.ID_comp =  comp.id AND projetcomp.ID_projet = ?;');
    $req->execute([$id]);
    return $req->fetchAll();



}     
function getAllTechno($id){
    global $pdo;
    $req = $pdo->prepare('SELECT techno.nom FROM techno, projettechno WHERE projettechno.ID_techno =  techno.id AND projettechno.ID_projet = ?;');
    $req->execute([$id]);
    return $req->fetchAll();

} 

function getOnlyTechno(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM techno");
    return $req->fetchAll();
};


function getOnlyComp(){
    global $pdo;
    $req = $pdo->query("SELECT * FROM comp");
    return $req->fetchAll();
}


function deleteTechno($effacer){

global $pdo; 
$delete = $pdo->prepare("DELETE FROM techno WHERE id = ?");
$delete->execute([$effacer]);
 }


 function deleteMail($effacer){
    global $pdo; 
    $delete = $pdo->prepare("DELETE FROM contact WHERE id = ?");
    $delete->execute([$effacer]);
     }

 function updateProject( $card_image,$card_title, $card_text,$modal_title,$img_modal,$modal_titre,$modal_text,$modal_date,$lien_git,$lien,$id){
    global $pdo;
    $req = $pdo->prepare('UPDATE cart  SET card_image = ?,card_title = ?, card_text = ? ,modal_title = ?,img_modal=?,modal_titre = ?, modal_text = ?, modal_date = ? ,lien_git=?, lien=?
    WHERE  id = ?');
    $req->execute([$card_image,$card_title, $card_text,$modal_title,$img_modal,$modal_titre,$modal_text,$modal_date,$lien_git ,$lien,$id]);

 };


 function getProjectId($id){
    global $pdo; 
    $req = $pdo->prepare('SELECT * FROM cart WHERE id= ?');
    $req->execute([$id]);
    return $req->fetchAll();
};

// function getCompId($id){
//     global $pdo; 
//     $req = $pdo->prepare('SELECT * FROM projetcomp WHERE ID_comp= ?');
//     $req->execute([$id]);
//     return $req->fetchAll();
// };

function InsertContact($name,$email,$comment){
    global $pdo;
    $req = $pdo->prepare("INSERT into contact (nom,email,comment) values (?,?,?);");
    $req->execute([$name,$email,$comment]);
};

function getAllMail(){ 
global $pdo;
$req = $pdo->query("SELECT * FROM contact");
return $req->fetchAll();
}
// header('Location:index.php');

?>