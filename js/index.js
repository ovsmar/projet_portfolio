/*функция для ползунок для перемотки с header вниз*/

let arrow = document.querySelector(".arrow");
arrow.addEventListener("click", () => {
  let about = document.querySelector("#section1");
  about.scrollIntoView({ behavior: "smooth", block: "start", inline: "start" });
});
/* */

var refreshButton = document.querySelector(".refresh-captcha");
refreshButton.onclick = function () {
  document.querySelector(".captcha-image").src =
    "../controller/captcha.php?" + Date.now();
};

function checkForm(form) {
  if (!form.captcha.value.match(/^\d{5}$/)) {
    alert("Please enter the CAPTCHA digits in the box provided");
    form.captcha.focus();
    return false;
  }

  return true;
}

$(document).ready(function () {
  $(".preloader").fadeOut(3100);
});
